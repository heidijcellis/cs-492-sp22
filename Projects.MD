# Projects - Spring 2022

## Project Guidelines
We will be using four projects this spring. The PlaceOrder project will be subdivided into three projects: 
* BackEnd
* FrontEnd
* API

The **InventorySystem** will be the third project. All projects must be structured similar to the examples found in the [LFP MicroServices training](https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples). Ignore the "back end for front end" and "back-end-for-front-end-code" projects. 

All projects must:
* Update from React to Vue if necessary
* Have correctly operating automated testing (Chai and Mocha) including a complete test suite
  * This may mean that there are unit tests that test only part of the code
  * Also need integration tests
* Be packaged into a separate, well-organized repository
  * Docker and docker compose files if necessary
* When a merge request is approved:
  * Run automated tests in the CI/CD pipeline to prevent merge requests from merging that do not pass the tests
  * Automate versioning and autorelease when a merge request is successfully merged so that versions are automatically generated
* Make all configurable items environment variables as is standard practice for containers
  * For instance, some of the infrastructure has hard-coded port numbers in files. You should be able to set the port number when the system is deployed via an environment variable 
* Dockerize the project and have the most recent image loaded into the project's Container Registry. 
* Docerize the development environment

## InventorySystem
This project will be stared from scratch and will require identifying the MVP that could be delivered by the end of the term. 

## PlaceOrder/API 
The API is for PlaceOrder is currently embedded in the code in the BackEnd. This code needs to be refactored and turned into a REST API project similar to the [API in the Microservices Example](https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/api/-/tree/main/). This will require a complete redesign to incorporate REST. A great resource to get started on APIs is: https://restfulapi.net/rest-api-design-tutorial-with-example/

## PlaceOrder/FrontEnd
This project will need to interface with the API project and refactor and update code to utilize the new REST api. 

## PlaceOrder/BackEnd
This project will need to interface with the API project and refactor and update code to utilize the new REST api. 
