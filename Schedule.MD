Week | Date | End Game & Spiderman | Sprint Choc Chip & Team Back End
:-- | :-- | :-- | :--
1 <br /> **H103**| 1/25 | Introduction and Team Formation | Introduction and Team Formation 
1 <br /> **H103**| 1/27 | Pre-Planning | Observe - send someone to observe End Game and Spiderman planning
2 <br /> **H103** | 2/1 | Continue planning in class  | Pre-Planning
2 <br /> **H103** | 2/3 | Sprint 1 Planning Meeting | Continue planning in class <br /> Send observer to End Game and Spiderman Planning Meeting 
3 <br /> **H103** | 2/8 | Continue working in class <br /> Send observer to Sprint Choc Chip and Team Back End Planning Meeting | Sprint 1 Planning Meeting
3 <br /> **H103** | 2/10 | Standup Meeting <br /> Status Report | Standup Meeting <br /> Status Report  
4 <br /> **H103** | 2/15 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
4 <br /> **H103** | 2/17 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
5 <br /> **H103**  | 2/22 | Sprint 1 Review <br /> Status Report  | Standup Meeting <br /> Status Report  <br /> Send observer to End Game and Spiderman Reviews 
5 <br /> **H103**  | 2/24 | Sprint 1 Retrospective | Standup Meeting <br /> Continue working in class 
6 <br /> **H103** | 3/1 | Sprint 2 Planning <br /> Send observer to Sprint Choc Chip and Team Back End Review  | Sprint 1 Review <br /> Continue working in class <br /> Send observer to End Game and Spiderman
6  <br />**Dr. Ellis gone** | 3/3 | Standup Meeting <br /> Continue working in class  | Sprint 1 Retrospective 
7 <br /> **H103** | 3/8 | Standup Meeting <br /> Continue working in class <br /> Send observer to Sprint Choc Chip and Back End Planning | Sprint 2 Planning Meeting
7 <br /> **H103**  | 3/10 | Standup Meeting <br /> Status Report   | Standup Meeting <br /> Status Report  
X | 3/13-20 | SPRING BREAK
8 <br /> **H103** | 3/22 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
8  <br /> **H103** | 3/24 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
9 <br /> **H103** | 3/29 | Standup Meeting <br /> Status Report | Standup Meeting <br /> Status Report 
9  <br /> **H103**  | 3/31 | Sprint 2 Review | Standup Meeting <br /> Continue working in class <br /> Send observer to End Game and Spiderman Reviews 
10  <br /> **Discord**  | 4/5| Sprint 2 Retrospective | Standup Meeting <br /> Continue working in class 
10  <br /> **H103** | 4/7| Sprint 3 Planning <br /> Send observer to Sprint Choc Chip and Team Back End Review  | Sprint 2 Review <br /> Send observer to End Game and Spiderman Planning 
11 <br /> **Discord** | 4/12 | Standup Meeting <br /> Continue working in class | Sprint 2 Retrospective
11 <br /> **H103** | 4/14 |Standup Meeting <br /> Send observer to Sprint Choc Chip and Team Back End Planning | Sprint 3 Planning
12 <br /> **Discord** | 4/19 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
12 <br /> **H103** | 4/21 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
13 <br /> **Discord** | 4/26 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
13 <br /> **H103** | 4/28 | Standup Meeting <br /> Status Report  | Standup Meeting <br /> Status Report 
14 <br /> **H103** | 5/3 | Sprint 3 Review | Standup Meeting <br /> Continue working in class <br /> Send observer to End Game and Spiderman Review
14 <br /> **H103** | 5/5 | Sprint 3 Retrospective | Sprint 3 Review and Retrospective 
15 | 5/9 | Midnight - Final Essay | Midnight - Final Essay - optional. If you choose not to do an essay, I will distribute these points among the three sprints. 

The table below shows the number of work sessions for each sprint for each team. This table is provided to help aid planning.

Sprint | Work Sessions<br> Teams 1 & 2 | Work Sessions <br> Teams 3 & 4
:--: | :--: | :--:
1 | 5 | 6
2 | 6 | 6
3 | 6 | 5
Total | 17 | 17

## Copyright and License
#### &copy; 2022 Heidi Ellis, Stoney Jackson and Karl Wurst
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019

