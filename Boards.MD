# GitLab Boards

## Board Organization
All projects shall set up a Developer issue board with the following lists, from left to right:
* **Backlog** – holds all issues that need to be completed.
* **To-Do** – holds all issues that need to be completed for the current sprint
* **In-Progress** – holds all issues that are currently being worked on
* **Review** – holds issues that have been completed and are awaiting review by instructor
* **Done** – holds issues that have been reviewed and approved by instructor
* **Team-Organization** - holds issues describing how the team operates including meeting times, process for changes to be merged, etc.  (Could be located in a Working Agreement issue.) This column may hold issues that do not conform to the Spike 1.0.0 template. This column also holds the Retrospectives, one per person. 
You may utilize additional columns, however these should not show up in the Developer board. 

## GitLab Board Set Up 
* Upon the start of the semester, each team must set up their board as described above. 
  * Any “open” issues must be evaluated to determine if they are still relevant. If so, they should be put into the Backlog. 
  * You may want to hide the “Open” and “Closed” issues after gardening your board. 
* The issues on your board should utilize the “Spike 1.0.0” template 
    * You must create a .gitlab directory at your project level in GitLab 
    * Copy the templates found in https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/Community/-/tree/main/.gitlab/issue_templates into this directory 
    * You should now see the Spike 1.0.0 template as an option when you create a new issue. 
    * An example of what this looks like is here: https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/authsystem/BNM/-/issues/3

## GitLab Board Use
1. Once the board has been set up, you must start adding issues to the Backlog. 
  * All issues should utilize the Spike 1.0.0 template. 
  * All issues should be first be located in the Backlog
2. Once you have identified the next steps to be taken in your project, you should move sufficient issues from Backlog to the To-Do column to keep all team members busy until the next Review meeting. 
  * This means that you should have issues assigned to individual team members with sufficient weights to occupy all team members from the Planning meeting until the Review meeting. (See Issue Weights below.)
3. You must a complete set of issues in the To Do column by 5 PM the day before your team’s Planning Meeting.
4. Prior to the Planning Meeting the instructor will review the issues in the To-Do column and either approve them or return them to Backlog for revision. 
5. As a team member starts to work on an issue, the issue should be moved from the To-Do column to the In-Progress column.
6. Once the issue has been completed, the issue should be moved into the Review column. 
7. All issues from the To-Do column must be completed by 5 PM the day before the Sprint Review meeting. 
8. The instructor will review the issues before the meeting.
9. During the Review meeting, you will summarize the progress of each issue. 
10. If the instructor is satisfied that the issue has been completed, the issue will be moved to the Done column. 

## Issue Weights
Weighting of issues is as follows using a point system using wall-clock time: 
* 0pt - 1/2 person-day
* 1pt - person-day
* 2pt - 1/2 person-week
* 4pt - person-week
* 8pt - 2 person-week
Assume that "day" and "week" are the normal amount of time you would put into a single course in a day and in a week. 
