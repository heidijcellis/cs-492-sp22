## Agenda Tuesday 3/22
**Everyone please attend class from the beginning so we can talk about integration.** 

### Standup Meetings: 
* 11:00 Team Spiderman
* 11:15 Team Sprint Chocolate Chip
* 11:30 Team Backend
* 11:45 Team End Game
