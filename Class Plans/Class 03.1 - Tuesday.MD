## Agenda Tuesday 2/8
* **11:00 Team Sprint Chocolate Chip** - Planning Meeting (All issues must be in To Do list by 5:00 PM Weds 2/2)
* **11:30 Team Backend**  - Planning Meeting (All issues must be in To Do list by 5:00 PM Weds 2/2)
*  Remaining teams work within your team. **Send a "spy" to both Planning Meetings.**

### Planning Meeting Agenda
* Walk through all tasks in "To-Do" column 
* Explain each task and weighting 
* Every issue must have a primary person
* Identify any dependencies in the work
* Ensure that there is sufficient work for all team members during the sprint
