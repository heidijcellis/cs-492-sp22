## Agenda
### 11:00 Spiderman
 * Two team members who have not yet presented will present next class - see outline below
 * Boards must be done by 5:00 PM Monday, May 2nd
 * Retro must be done by 5:00 PM Wednesday, May 4th
### 11:15 Backend
  * Two team members who have not yet presented will present next Thursday - see outline below
  * Both Boards and Retro are due 5:00 PM Wednesday, May 4th
### 11:30 Endgame
 * Two team members who have not yet presented will present next class - see outline below
 * Boards must be done by 5:00 PM Monday, May 2nd
 * Retro must be done by 5:00 PM Wednesday, May 4th
### 11:45 Sprint Chocolate Chip
  * Two team members who have not yet presented will present next Thursday - see outline below
  * Both Boards and Retro are due 5:00 PM Wednesday, May 4th

### Presentations: 
* Two presentations per team: 
    * One person presents the current status of the project 
        * 10 minutes 
        * Overview of your segment of the project 
            * How does it fit into the larger picture of the BNM project?
        * Identification of the major tasks involved in the sprint 
            * Degree of completeness of each task 
        * How well positioned is the project to be picked up in the fall?
        * What was your big "take away" from this sprint?
        * What was your big "take away" from the class?
    * One person presents the future of the project 
        * 10 minutes 
        * Demo/show current state of project
        * What roadblocks were encountered? 
        * How will these roadblocks be addressed in the future? 
        * What are the next steps in the work? 
        * What was your big "take away" from this sprint?
        * What was your big "take away" from the class?

