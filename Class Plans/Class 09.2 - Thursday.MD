## Agenda - Thursday 3/31
### 11:00 Team Spiderman Review Meeting 
### 11:30 Team End Game Review Meeting

* Two presentations per team: 
    * One person presents the current status of the project 
        * 10 minutes 
        * Overview of your segment of the project 
            * How does it fit into the larger picture of the BNM project?
        * Identification of the major tasks involved in the sprint 
            * Degree of completeness of each task 
            * What is left to be done 
        * What was your big "take away" from this sprint?
    * One person presents the future of the project 
        * 10 minutes 
        * Demo/show current state of project
        * What roadblocks were encountered? 
        * How will these roadblocks be addressed in the future? 
        * What are the next steps in the work? 
        * What was your big "take away" from this sprint?

Teams Backend and Sprint Chocolate Chip have stand-ups on your own.  Make sure to send a "spy" to the Review meetings.

**Spiderman and End Game team members must individually answer [retrospective questions](https://gitlab.com/heidijcellis/cs-492-sp22/-/blob/master/Sprint-Evaluation.MD) publicly in an issue titled Retrospective - <LastName> in the Working Agreement column on the team board by 5:00 PM Monday 4/4**
